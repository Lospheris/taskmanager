package com.example.taskmanager

import java.io.Serializable
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

//----------------------------Convenience Variables-------------------------------------------------
const val EXTRA_TASK = "task"

//--------------------------Task Class---------------------------------------------------------

class Task(
    val id: Int = 0,
    var name: String = "None",
    var shortDescription: String = "None",
    var longDescription: String = "None",
    var dateDue: LocalDateTime = LocalDateTime.now().plusYears(1),
    var priority: Priority = Priority.Low,
    var lastUpdated: LocalDateTime = LocalDateTime.now(),
    //var subTasks = mutableListOf<Task>()
    var isCompleted: Boolean = false,
    var isArchived: Boolean = false
): Serializable {

    override fun toString(): String {
        return "ID: $id\n" +
                "Name: $name\n" +
                "Short Description: $shortDescription\n" +
                "Long Description: $longDescription\n" +
                "Date Due: ${dateDue.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}\n" +
                "Priority: $priority\n" +
                "Last Updated: ${lastUpdated.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}\n" +
                "Completed: $isCompleted\n" +
                "Archived: $isArchived"
    }

}

enum class Priority{
    Low,
    Medium,
    High,
    RTFN;
}