package com.example.taskmanager

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_view_task.*
import java.text.SimpleDateFormat
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*


class ViewTaskActivity : AppCompatActivity() {

    //---------------Buttons----------------------------
    private var btnCancel: Button? = null
    private var btnUpdate: Button? = null

    //---------------Edit Texts-------------------------
    private var etName: EditText? = null
    private var etShortDescription: EditText? = null
    private var etLongDescription: EditText? = null
    private var etDateDue: EditText? = null
    private var etTimeDue: EditText? = null

    //---------------Text Views-------------------------
    private var tvLastUpdated: TextView? = null

    //---------------Check Boxes------------------------
    private var cbTaskCompleted: CheckBox? = null
    private var cbTaskArchived: CheckBox? = null

    //---------------One Spinner------------------------
    private var spnrPriority: Spinner? = null

    //---------------Date Times-------------------------
    //var ldtDateDue: LocalDateTime? = null
    //var ldtLastUpdated: LocalDateTime? = null
    private var cal: Calendar = Calendar.getInstance()

    //---------------A place to put the task-----------
    private var task: Task? = null

    //---------------Is this a new task?---------------
    private var isNew: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_task)

        //-----------------Get UI Elements from the Layout----------------
        btnCancel = this.btnViewTaskCancel
        btnUpdate = this.btnViewTaskUpdate

        etName = this.etViewTaskName
        etShortDescription = this.etViewTaskShortDescription
        etLongDescription = this.etViewTaskLongDescription
        etDateDue = this.etViewTaskDate
        etTimeDue = this.etViewTaskTime

        spnrPriority = this.spnrViewTaskPriority

        tvLastUpdated = this.tvViewTaskLastUpdated

        cbTaskCompleted = this.cbViewTaskCompleted
        cbTaskArchived = this.cbViewTaskArchived

        if (!intent.hasExtra(EXTRA_TASK)) {

            // If we didn't have the task extra, then this is a new task
            //finishNoExtras()
            task = Task()
            isNew = true
            btnUpdate!!.text = getString(R.string.add_task)

        } else {
            task = this.intent.getSerializableExtra(EXTRA_TASK) as? Task
        }

        //-----------------OnClick Listeners-------------------------------
        btnCancel!!.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        btnUpdate!!.setOnClickListener {
                if (isPopulated()) {
                val result = Intent()
                prepareTaskForIntent()
                result.putExtra(EXTRA_TASK, task)
                setResult(Activity.RESULT_OK, result)
                finish()
            } else {
                Toast.makeText(this@ViewTaskActivity,
                    "You must set a task name, short description, long description, due date, and time.",
                    Toast.LENGTH_LONG).show()
            }
        }
        if (!isNew) {
            //------------------Set the date/time/cal objects-----------------------
            /*cal.set(
                task!!.dateDue.year, task!!.dateDue.monthValue,
                task!!.dateDue.dayOfMonth, task!!.dateDue.hour,
                task!!.dateDue.minute
            )*/
            cal.clear()
            cal.time = Date.from(task!!.dateDue.atZone(ZoneId.systemDefault()).toInstant())

            //-----------------Assign the Values from the Task-----------------
            etName!!.setText(task!!.name)
            etShortDescription!!.setText(task!!.shortDescription)
            etLongDescription!!.setText(task!!.longDescription)
            /*tvLastUpdated!!.text = "Last Updated: " +
                    task!!.lastUpdated.format(
                        DateTimeFormatter.ofPattern("dd MMM yyyy 'at' HH:mm")
                    )*/
            tvLastUpdated!!.text = getString(R.string.last_updated_display,
                                            task!!.lastUpdated.format(
                                            DateTimeFormatter.ofPattern(
                                            "dd MMM yyyy 'at' HH:mm")))
            spnrPriority!!.setSelection(priorityToSpinnerValue())
            updateDateInView()
            updateTimeInView()

            cbTaskCompleted!!.isChecked = task!!.isCompleted
            cbTaskArchived!!.isChecked = task!!.isArchived
        }

        //------------------------Date/Time Pickers-----------------------
        // This is the actual datepicker
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }

        // this is the actual time picker
        val timeSetListener =
            TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hourOfDay)
                cal.set(Calendar.MINUTE, minute)
                updateTimeInView()
            }

        //------------------------Date/Time OnClickListeners--------------
        // Listen for clickery on the date field
        etDateDue!!.setOnClickListener {
            DatePickerDialog(this@ViewTaskActivity,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show()
        }

        // Listen for time clickery
        etTimeDue!!.setOnClickListener {
            TimePickerDialog(this@ViewTaskActivity, timeSetListener,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                true).show()
        }

    }

    // This function is called by the date picker to update the text field for the date.
    private fun updateDateInView() {
        val myFormat = "dd MMM YYYY" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        etDateDue!!.setText(sdf.format(cal.time))
    }

    // This function is called by the date picker to update the text field for the date.
    private fun updateTimeInView() {
        val myFormat = "HH:mm" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        etTimeDue!!.setText(sdf.format(cal.time))
    }

    // Spinner value to priority
    private fun spinnerValueToPriority(): Priority {

        when(spnrPriority?.selectedItem.toString().toLowerCase()){
            "low" -> return Priority.Low
            "medium" -> return Priority.Medium
            "high" -> return Priority.High
            "rtfn" -> return Priority.RTFN
        }
        return Priority.Low
    }

    // Priority to spinner index
    private fun priorityToSpinnerValue(): Int {
        return when(task?.priority) {
            Priority.Low -> 0
            Priority.Medium -> 1
            Priority.High -> 2
            Priority.RTFN -> 3
            else -> 0
        }
    }
    // Check and make sure we have the all the right fields populated
    private fun isPopulated(): Boolean {
        return etName!!.text.isNotEmpty() && etShortDescription!!.text.isNotEmpty() &&
                etLongDescription!!.text.isNotEmpty() && etDateDue!!.text.isNotEmpty() &&
                etTimeDue!!.text.isNotEmpty()
    }

    // a function to prepare the task for return
    private fun prepareTaskForIntent() {
        task?.name = etName!!.text.toString()
        task?.shortDescription = etShortDescription!!.text.toString()
        task?.longDescription = etLongDescription!!.text.toString()
        task?.isCompleted = cbTaskCompleted!!.isChecked
        task?.isArchived = cbTaskArchived!!.isChecked
        //task?.dateDue = LocalDateTime.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                                        //cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR),
                                        //cal.get(Calendar.MINUTE))
        task?.dateDue = cal.time.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()
        task?.priority = spinnerValueToPriority()
    }
}
