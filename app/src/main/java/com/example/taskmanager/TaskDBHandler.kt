package com.example.taskmanager

import android.content.ContentValues
import android.content.Context
import android.database.DatabaseUtils
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.time.LocalDateTime
import java.time.LocalDateTime.now
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME


class TaskDBHandler(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME,
                    factory, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val createTaskTable = ("CREATE TABLE " +
                TABLE_TASKS + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY," +
                COLUMN_TASKNAME + " TEXT," + COLUMN_TASKSHORTDESCRIPTION + " TEXT," +
                COLUMN_TASKLONGDESCRIPTION + " TEXT," + COLUMN_TASKDATEDUE + " TEXT," +
                COLUMN_TASKPRIORITY + " INTEGER," + COLUMN_TASKLASTUPDATED + " TEXT," +
                COLUMN_TASKCOMPLETED + " INTEGER," + COLUMN_TASKARCHIVED + " INTEGER" +
                ")")
        db.execSQL(createTaskTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int,
                           newVersion: Int) {
        if ( newVersion > oldVersion ) {
            db.execSQL("DROP TABLE IF EXISTS $TABLE_TASKS")
            onCreate(db)
        }
    }

    fun deleteDatabase() {
        val db = this.writableDatabase
        db.execSQL("DROP TABLE IF EXISTS $TABLE_TASKS")
        onCreate(db)
    }

    fun addTask(task: Task) {

        // Create a place to put the values
        val values = ContentValues()

        // Populate the values
        values.put(COLUMN_TASKNAME, task.name)
        values.put(COLUMN_TASKSHORTDESCRIPTION, task.shortDescription)
        values.put(COLUMN_TASKLONGDESCRIPTION, task.longDescription)
        values.put(COLUMN_TASKDATEDUE, task.dateDue.format(ISO_LOCAL_DATE_TIME))
        values.put(COLUMN_TASKPRIORITY, task.priority.ordinal)
        values.put(COLUMN_TASKLASTUPDATED, task.lastUpdated.format(ISO_LOCAL_DATE_TIME))
        values.put(COLUMN_TASKCOMPLETED, task.isCompleted)
        values.put(COLUMN_TASKARCHIVED, task.isArchived)

        // Open the database, write the values, and close the database.
        val db = this.writableDatabase
        db.insert(TABLE_TASKS, null, values)
        db.close()

    }

    fun getTasks(isCompleted: Boolean = false,
                 isArchived: Boolean = false): MutableList<Task> {

        // A place to put the tasks
        val tasks: MutableList<Task> = mutableListOf()

        var query = "SELECT * FROM " + TABLE_TASKS + " WHERE"

        when(isCompleted) {

            true -> if(!isArchived) query += " $COLUMN_TASKCOMPLETED=1 AND"
            false -> query += " NOT $COLUMN_TASKCOMPLETED AND"
        }

        query += when(isArchived) {
            true -> " $COLUMN_TASKARCHIVED=1"
            false -> " NOT $COLUMN_TASKARCHIVED"
        }

        val db = this.writableDatabase

        val cursor = db.rawQuery(query, null)

        //Log.d("TASKMAN", DatabaseUtils.dumpCursorToString(cursor))
        if (cursor.moveToFirst()) {
            while(!cursor.isAfterLast) {
                val id = Integer.parseInt(cursor.getString(0))
                val name = cursor.getString(1)
                val shortDescription = cursor.getString(2)
                val longDescription = cursor.getString(3)
                val dateDue = cursor.getString(4)
                val priority = Integer.parseInt(cursor.getString(5))
                val lastUpdate = cursor.getString(6)
                val completed: Boolean = (Integer.parseInt(cursor.getString(7)) == 1)
                val archived: Boolean = (Integer.parseInt(cursor.getString(8)) == 1)
                tasks.add(
                    Task(
                        id, name, shortDescription, longDescription,
                        LocalDateTime.parse(dateDue, ISO_LOCAL_DATE_TIME),
                        convertIntToPriority(priority),
                        LocalDateTime.parse(lastUpdate, ISO_LOCAL_DATE_TIME), completed, archived
                    )
                )
                cursor.moveToNext()
            }
            cursor.close()
        }
        db.close()
        return tasks
    }

    fun deleteTask(id: Int): Boolean {

        val result: Boolean
        val db = this.writableDatabase
        result = db.delete(TABLE_TASKS, "$COLUMN_ID=$id", null ) > 0
        db.close()
        return result
    }

    fun completeTask(id: Int): Boolean {

        // Create a place to put the values to send the update!
        val values = ContentValues()
        values.put(COLUMN_TASKCOMPLETED, true)

        // We need to store the return value.
        val returnValue: Boolean

        // Because you have to pass an array of strings as where args, we need to make an array
        val whereArgs = arrayOf(java.lang.String.valueOf(id))

        // Open the database for writey business
        val db = this.writableDatabase

        // Try to do the damn thing
        returnValue = db.update(TABLE_TASKS, values, "_id=?", whereArgs) > 0

        // Close the database
        db.close()

        // Send the value of a thing to a place
        return returnValue
    }

    fun archiveTask(id: Int): Boolean {

        // Create a place to put the values to send the update!
        val values = ContentValues()
        values.put(COLUMN_TASKARCHIVED, true)

        // We need to store the return value.
        val returnValue: Boolean

        // Because you have to pass an array of strings as where args, we need to make an array
        val whereArgs = arrayOf(java.lang.String.valueOf(id))

        // Open the database for writey business
        val db = this.writableDatabase

        // Try to do the damn thing
        returnValue = db.update(TABLE_TASKS, values, "_id=?", whereArgs) > 0

        // Close the database
        db.close()

        // Send the value of a thing to a place
        return returnValue
    }

    fun updateTask(task: Task): Boolean {

        // A place for values to go for the update
        val values = ContentValues()
        values.put(COLUMN_TASKNAME, task.name)
        values.put(COLUMN_TASKSHORTDESCRIPTION, task.shortDescription)
        values.put(COLUMN_TASKLONGDESCRIPTION, task.longDescription)
        values.put(COLUMN_TASKDATEDUE, task.dateDue.format(ISO_LOCAL_DATE_TIME))
        values.put(COLUMN_TASKPRIORITY, task.priority.ordinal)
        values.put(COLUMN_TASKLASTUPDATED, now().format(ISO_LOCAL_DATE_TIME))
        values.put(COLUMN_TASKCOMPLETED, task.isCompleted)
        values.put(COLUMN_TASKARCHIVED, task.isArchived)

        // We need to store the return value.
        val returnValue: Boolean

        // Because you have to pass an array of strings as where args, we need to make an array
        val whereArgs = arrayOf(java.lang.String.valueOf(task.id))

        // Open the database for writey business
        val db = this.writableDatabase

        // Try to do the damn thing
        returnValue = db.update(TABLE_TASKS, values, "_id=?", whereArgs) > 0

        // Close the database
        db.close()

        // Send the value of a thing to a place
        return returnValue
    }

    fun debugDisplayDatabase(): String {
        // Define the query. This is very debuggy so it's just everything for now.
        val query = "SELECT * FROM $TABLE_TASKS"

        // get a readable database. We don't really need to write here.
        val db = this.readableDatabase

        // Query the database and get the cursor.
        val cursor = db.rawQuery(query, null)

        // Dump a stringed version of the table to return
        return DatabaseUtils.dumpCursorToString(cursor)
    }

    companion object {

        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "taskDB.db"
        const val TABLE_TASKS = "tasks"

        const val COLUMN_ID = "_id"
        const val COLUMN_TASKNAME = "taskname"
        const val COLUMN_TASKSHORTDESCRIPTION = "taskshortdescription"
        const val COLUMN_TASKLONGDESCRIPTION = "tasklongdescription"
        const val COLUMN_TASKDATEDUE = "taskdatedue"
        const val COLUMN_TASKPRIORITY = "taskpriority"
        const val COLUMN_TASKLASTUPDATED = "tasklastupdated"
        const val COLUMN_TASKCOMPLETED = "taskcompleted"
        const val COLUMN_TASKARCHIVED = "taskarchived"
    }

    private fun convertIntToPriority(pri: Int): Priority {
        when(pri) {
            0 -> return Priority.Low
            1 -> return Priority.Medium
            2 -> return Priority.High
            3 -> return Priority.RTFN
        }
        return Priority.Low
    }
}