package com.example.taskmanager

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import java.time.LocalDateTime
import java.time.LocalDateTime.now
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit.DAYS


class TaskAdapter (private val context: Context, private val tasks: MutableList<Task>):
    BaseAdapter() {

    override fun getCount(): Int {
        return tasks.size
    }

    override fun getItem(position: Int): Any {
        return tasks[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertViewIn: View?, parent: ViewGroup): View {
        var convertView = convertViewIn
        val holder: ViewHolder

        if (convertView == null) {
            holder = ViewHolder()
            val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.task_item, null, true)


            holder.taskName = convertView!!.findViewById(R.id.etNewTaskName) as TextView
            holder.taskShortDescription =
                convertView.findViewById(R.id.etNewTaskShortDescription) as TextView
            holder.dateDue = convertView.findViewById(R.id.tvDateDue) as TextView
            holder.dateUpdated = convertView.findViewById(R.id.tvDateUpdated) as TextView

            convertView.tag = holder
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = convertView.tag as ViewHolder
        }

        holder.taskName!!.text = tasks[position].name

        holder.taskShortDescription!!.text = tasks[position].shortDescription

        holder.dateDue!!.text = tasks[position].dateDue.format(
            DateTimeFormatter.ofPattern("dd MMM yyyy 'at' HH:mm"))

        holder.dateUpdated!!.text = tasks[position].lastUpdated.format(
            DateTimeFormatter.ofPattern("dd MMM yyyy 'at' HH:mm"))

        /*
            This is the code to change the color of task names based on priority, and
            due dates based on temporal proximity. It should not fire when the task
            is archived or completed. I also had this weird problem when views were
            being recycled the colors would be held over from the last task to use
            the view. So I'm setting them to "default" here before changing them again.
         */
        holder.taskName!!.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
        holder.dateDue!!.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
        when (tasks[position].isArchived || tasks[position].isCompleted) {
            false -> {
                holder.taskName!!.setTextColor(
                    updateNameColorFromPriority(
                        tasks[position].priority))
                holder.dateDue!!.setTextColor(updateDateDueColorFromNow(tasks[position].dateDue))
            }
        }

        return convertView
    }

    override fun isEnabled(position: Int): Boolean {
        return true
    }

    private inner class ViewHolder {

        var taskName: TextView? = null
        var taskShortDescription: TextView? = null
        var dateDue: TextView? = null
        var dateUpdated: TextView? = null
    }

    private fun updateNameColorFromPriority(priority: Priority): Int {

        return when(priority) {
            Priority.Low -> ContextCompat.getColor(context, R.color.colorPrimaryDark)
            Priority.Medium -> ContextCompat.getColor(context, R.color.medium_priority)
            Priority.High -> ContextCompat.getColor(context, R.color.high_priority)
            Priority.RTFN -> ContextCompat.getColor(context, R.color.rtfn_priority)
        }

    }

    private fun updateDateDueColorFromNow(dateDue: LocalDateTime): Int {

        return when (DAYS.between(now(), dateDue)) {
            in Int.MIN_VALUE..0 -> ContextCompat.getColor(context, R.color.rtfn_priority)
            in 1..2 -> ContextCompat.getColor(context, R.color.high_priority)
            in 3..7 -> ContextCompat.getColor(context, R.color.medium_priority)
            else -> ContextCompat.getColor(context, R.color.low_priority)
        }
    }
}