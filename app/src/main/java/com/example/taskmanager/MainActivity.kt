package com.example.taskmanager

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.Button
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


const val NEW_TASK_INTENT_RETURN = 1
const val VIEW_TASK_INTENT_RETURN = 2

class MainActivity : AppCompatActivity() {

    // Initialize the database handler
    private val dbHandler = TaskDBHandler(this, null )

    // Created the tasklist
    private var taskList = mutableListOf<Task>()

    // A place to hold my button
    private var newButton: Button? = null
    private var openButton: Button? = null
    private var closedButton: Button? = null
    private var archivedButton: Button? = null


    // Some bools to describe what tasks to show.
    private var showCompleted: Boolean = false
    private var showArchived: Boolean = false

    // adapter and list view
    private var adapter: TaskAdapter? = null
    private var listView: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Get and set buttons to local values so we can handle clicks
        newButton = this.btnNew
        openButton = this.btnActive
        closedButton = this.btnComplete
        archivedButton = this.btnArchive

        //dbHandler.deleteDatabase()
        taskList = this.dbHandler.getTasks(showCompleted, showArchived)

        // Currently just to prevent issues if the database is empty for testing.
        if ( taskList.count() < 1 ) {
            dbHandler.addTask(Task(0, "New Task"))
            taskList = dbHandler.getTasks( false, isArchived = false)
        }

        /* Create the task adapter */
        adapter = TaskAdapter(this, taskList)

        // Create the ListView
        listView = findViewById(R.id.lvTaskList)

        // Set onitem click listener
        listView!!.setOnItemClickListener { _, _, position, _ ->
            val intent = Intent(this@MainActivity, ViewTaskActivity::class.java)
            intent.putExtra("task", taskList[position])
            this.startActivityForResult(intent, VIEW_TASK_INTENT_RETURN)
        }


        // Set the adapter to the view
        listView!!.adapter = adapter

        // Set the listView Context Menu
        registerForContextMenu(listView)

        // ---------------------- On Click Listeners -------------------------------------------
        newButton!!.setOnClickListener {
            val intent = Intent(this@MainActivity, ViewTaskActivity::class.java)
            startActivityForResult(intent, NEW_TASK_INTENT_RETURN)
        }

        openButton!!.setOnClickListener {
            showCompleted = false
            showArchived = false
            updateTasksAndPopulateListView()
        }

        closedButton!!.setOnClickListener {
            //Log.d("TASKMAN",dbHandler.debugDisplayDatabase())
            showCompleted = true
            showArchived = false
            updateTasksAndPopulateListView()
        }

        archivedButton!!.setOnClickListener {
            showCompleted = true
            showArchived = true
            updateTasksAndPopulateListView()
        }
    } // End onCreate

//---------------------------Activity Result Handler------------------------------------------------
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // This is required
        super.onActivityResult(requestCode, resultCode, data)

        // Check which request we're responding to
        if (requestCode == NEW_TASK_INTENT_RETURN) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                if (data!!.hasExtra(EXTRA_TASK)) {
                    val task = data!!.getSerializableExtra(EXTRA_TASK) as? Task
                    dbHandler.addTask(task!!)
                    updateTasksAndPopulateListView()
                }

            }
        }
        if (requestCode == VIEW_TASK_INTENT_RETURN) {
            if (resultCode == Activity.RESULT_OK) {
                if (data!!.hasExtra(EXTRA_TASK)) {
                    val task = data!!.getSerializableExtra(EXTRA_TASK) as? Task
                    dbHandler.updateTask(task!!)
                    updateTasksAndPopulateListView()
                }
            }
        }
    } // End onActivityResult

    // A helper function for getting tasks
    private fun updateTasksAndPopulateListView() {
        taskList.clear()
        taskList.addAll(dbHandler.getTasks(showCompleted, showArchived))
        adapter!!.notifyDataSetChanged()
    }

    // Context menu function
    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View,
        menuInfo: ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        if (v.id == R.id.lvTaskList) {
            val inflater: MenuInflater = menuInflater
            inflater.inflate(R.menu.context_menu_values, menu)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterContextMenuInfo
        return when (item.itemId) {
            R.id.menu_complete ->  // add stuff here
                completeTaskFromContextMenu(taskList[info.position].id)
            R.id.menu_archive ->  // edit stuff here
                archiveTaskFromContextMenu(taskList[info.position].id)
            R.id.menu_delete ->  // remove stuff here
                deleteTaskFromContextMenu(taskList[info.position].id)
            R.id.menu_about -> {
                val intent = Intent(this@MainActivity, About::class.java)
                this.startActivity(intent)
                return true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    private fun deleteTaskFromContextMenu(id: Int): Boolean {

        // Delete the task from the database and get the result
        val result = dbHandler.deleteTask(id)

        // Refresh the listview
        updateTasksAndPopulateListView()

        return result
    }

    private fun completeTaskFromContextMenu(id: Int): Boolean {

        // Delete the task from the database and get the result
        val result = dbHandler.completeTask(id)

        // Refresh the listview
        updateTasksAndPopulateListView()

        return result
    }

    private fun archiveTaskFromContextMenu(id: Int): Boolean {

        // Delete the task from the database and get the result
        val result = dbHandler.archiveTask(id)

        // Refresh the listview
        updateTasksAndPopulateListView()

        return result
    }
    // delete and item
}





